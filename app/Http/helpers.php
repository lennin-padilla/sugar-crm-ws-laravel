<?php

namespace App\Http;

use Asakusuma\SugarWrapper\Rest;
use wouterNL\Drip\DripPhp;

class Helpers
{
    /**
     * Verifies if an email exist in contacts module
     *
     * @param $email
     * @return bool
     */
    public static function sugarcrm_contacts_module_check_email_exist($email)
    {
        $sugar = self::sugarcrm_connect();
        $email_escaped = strtoupper(addslashes($email));
        $filter_by_email = "
        contacts.id IN (
        SELECT eabr_scauth.bean_id
        FROM email_addr_bean_rel AS eabr_scauth
 
        INNER JOIN email_addresses AS ea_scauth
        ON ea_scauth.deleted = 0
        AND eabr_scauth.email_address_id = ea_scauth.id
        AND ea_scauth.email_address_caps = '$email_escaped'
 
        WHERE eabr_scauth.deleted = 0
        AND eabr_scauth.bean_module = 'Contacts'
        AND eabr_scauth.primary_address = 1
        )";

        return $sugar->count_records("Contacts", $filter_by_email);
    }

    /**
     * Allows to search in a module by a sql given
     *
     * @param $module_name
     * @param array $fields_to_display
     * @param $sql
     * @return array
     */
    public static function sugarcrm_search_by_module_name_and_sql($module_name, $fields_to_display = array(), $sql)
    {
        $sugar = self::sugarcrm_connect();

        return $sugar->get($module_name, $fields_to_display, ["limit" => 200, "where" => "$sql"]);
    }

    /**
     * Prepare contact data for sugarcrm contact module
     *
     * @param $data
     * @return array
     */
    public static function sugarcrm_prepare_contact_data($data)
    {
        if (isset($data['custom_fields']['Address'])) {
            $address = explode("\n", $data['custom_fields']['Address']);
            $street_address = $address[0];
            $city = explode(",", $address[1]);
            $state = explode(" ", ltrim($city[1]));
            $zip_code = $state[1];
            $state = $state[0];
            $city = $city[0];
        } else {
            $street_address = '';
            $city = '';
            $state = '';
            $zip_code = '';
        }
        $phone = isset($data['custom_fields']['Phone']) ? isset($data['custom_fields']['Phone']) : '';

        return [
            ["name" => "first_name", "value" => $data['custom_fields']['First_Name']],
            ["name" => "last_name", "value" => $data['custom_fields']['Last_Name']],
            ["name" => "email1", "value" => $data['email']],
            ["name" => "phone_work", "value" => $phone],
            ["name" => "primary_address_street", "value" => trim($street_address)],
            ["name" => "primary_address_city", "value" => trim($city)],
            ["name" => "primary_address_state", "value" => trim($state)],
            ["name" => "primary_address_postalcode", "value" => trim($zip_code)],
            ["name" => "primary_address_country", "value" => "USA"],
            ["name" => "drip_identifier_c", "value" => $data['id']],
        ];
    }

    /**
     * Create contact in sugarcrm
     *
     * @param $data
     * @return array
     */
    public static function sugarcrm_create_contact($data)
    {
        $sugar = self::sugarcrm_connect();

        return $sugar->set("Contacts", $data);
    }

    /**
     * Prepare subscriber format for drip
     *
     * @param $table_fields
     * @param $sugar_crm_fields
     * @return array
     */
    public static function prepare_drip_subscriber_format($table_fields, $sugar_crm_fields)
    {
        $sugar_crm_fields = $sugar_crm_fields[0];
        $address = "{$sugar_crm_fields['billing_address_street']}\n{$sugar_crm_fields['billing_address_city']}, ";
        $address .= "{$sugar_crm_fields['billing_address_state']} {$sugar_crm_fields['billing_address_postalcode']}";

        return [
            'email' => $table_fields->email,
            'custom_fields' => [
                'First_Name' => $table_fields->first_name,
                'Last_Name' => $table_fields->last_name,
                'Address' => $address,
                'Phone' => $sugar_crm_fields['phone_office'],
            ],
        ];
    }

    /**
     * Create drip subscriber
     *
     * @param $params
     * @return array|bool
     */
    public static function create_drip_subscriber($params)
    {
        $drip = new DripPhp('ec94e9d56404d9788d016fe09261dc24', '3134701');

        return $drip->createOrUpdateSubscriber($params);
    }

    /**
     * Get all drip subscribers
     *
     * @param $per_page
     * @return array|bool
     */
    public static function get_drip_subscribers($per_page)
    {
        $drip = new DripPhp('ec94e9d56404d9788d016fe09261dc24', '3134701');
        $response = $drip->makeRequest('https://api.getdrip.com/v2/3134701/subscribers', ['per_page' => $per_page]);

        if (!empty($response['buffer'])) {
            $raw_json = json_decode($response['buffer'], true);
        }

        $data = empty($raw_json)
            ? false
            : empty($raw_json['subscribers'])
                ? array()
                : $raw_json['subscribers'];

        return $data;
    }

    /**
     * Establish sugarcrm connection
     *
     * @return array|Rest|bool
     */
    private static function sugarcrm_connect()
    {
        $sugar = new Rest();

        $sugar->setUrl('http://datatest.preferati.net/service/v4_1/rest.php');
        $sugar->setUsername('aspirant');
        $sugar->setPassword('Check@t11#');

        $sugar->connect();

        $error = $sugar->get_error();

        if ($error !== false) {
            return $error;
        }

        return $sugar;
    }
}