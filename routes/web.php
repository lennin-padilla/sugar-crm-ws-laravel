<?php

use App\Contact;
use App\Http\Helpers;
use Edujugon\Log\Facades\Log;

//Step 1: use it just once
Route::get('/import-csv-data', function () {
    $file = storage_path('../').'/contacts.csv';
    $handle = fopen($file, "r");
    $header = true;

    while ($csvLine = fgetcsv($handle, 1000, ",")) {
        if ($header) {
            $header = false;
        } else {
            Contact::create(
                [
                    'first_name' => $csvLine[0],
                    'last_name' => $csvLine[1],
                    'email' => $csvLine[2],
                ]
            );
        }
    }
});

//Step 2 and 3
Route::get('/api-calls', function () {
    $contacts = Contact::all();
    $accounts_fields = [
        'phone_office',
        'billing_address_street',
        'billing_address_city',
        'billing_address_state',
        'billing_address_postalcode',
        'billing_address_country',
    ];

    collect($contacts)->map(function ($item) use ($accounts_fields) {
        $result = Helpers::sugarcrm_search_by_module_name_and_sql(
            "Accounts",
            $accounts_fields,
            "accounts.name = '".addslashes($item->last_name)."'"
        );

        if (collect($result)->count()) {
            $subscriber = Helpers::prepare_drip_subscriber_format($item, $result);
            $drip_subscriber = Helpers::create_drip_subscriber($subscriber);
            $item->drip_identifier = $drip_subscriber['id'];
            $item->save();
        } else {
            Log::fileName('missing-crm-accounts')
                ->title('Missing CRM account')
                ->line($item->last_name)
                ->line($item->email)
                ->days(3)
                ->write();
        }
    });
});

//Step 4
Route::get('/drip-subscribers', function () {
    $per_page = 1000;
    $drip_subscribers = Helpers::get_drip_subscribers($per_page);

    collect($drip_subscribers)->map(function($item) {
        $email_exist = Helpers::sugarcrm_contacts_module_check_email_exist($item['email']);

        if ($email_exist) {
            Log::fileName('existing-crm-contacts')
                ->title('Existing CRM contacts')
                ->line($item['custom_fields']['First_Name'])
                ->line($item['custom_fields']['Last_Name'])
                ->line($item['email'])
                ->days(3)
                ->write();
        } else {
            $new_contact = Helpers::sugarcrm_prepare_contact_data($item);
            Helpers::sugarcrm_create_contact($new_contact);
        }
    });
});
